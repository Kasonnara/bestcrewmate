# BestCrewMate

Unofficial 3rd player bot for the board game The Crew.

This project aims at providing a player bot for the board game [The Crew: The Quest for Planet Nine](https://www.kosmosgames.co.uk/games/the-crew-quest-for-planet-nine/). As the game became interresting only with 3 players or more, you may have been in the situation where you miss some players. That why we develop the BestCrewMate, to supply for one or more extra player to complete our games.  

## Installation


```bash
git clone https://gitlab.com/Kasonnara/bestcrewmate.git
cd bestcrewmate

# I recommend to make a python virtual env: 
#   if you doesn't have installed venv yet:
sudo apt install python3-venv
python3 -m venv bcm_env
# then activate it with 
source bcm_env/bin/activate

pip install -r requirements.txt 
#TODO
```

## Usage

```bash
cd main
python3 run_bestcrewmate_gui.py
```
Then go to [http://127.0.0.1:8050/](http://127.0.0.1:8050/)

## Roadmap
- [ ] core
- [ ] web user interface
- strategies
  - [ ] human controlled
  - [ ] reenforcement lerning dev env
  - [ ] supervised learning dev env
  - [ ] naive minimax
  - [ ] neural networks
  - [ ] random forest
- [ ] card recognition using a webcam

## Contributing

For us, this project is also a way to learn Python, web development and IA. 
Any contribution is welcome, but we're beginners, so please be comprehensive.

## License

General Public License (GPL)

Copyright (C) 2021  Kasonnara bcm@kasonnara.fr

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

## Project status

Under development


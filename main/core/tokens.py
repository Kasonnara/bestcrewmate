#!/usr/bin/python3
# -*- coding: utf-8 -*-

# BestCrewMate: a bot player for the board game The Crew 
# Copyright (C) 2021  Kasonnara <bcm@kasonnara.fr>
# GNU General Public License version 3 

"""
Representation of physical game card tokens
"""
import os
from enum import unique, Enum, auto

TOKENS_ASSETS_FOLDER = 'tokens'


@unique
class CardToken(Enum):
    CHEVRON_1 = auto()
    CHEVRON_2 = auto()
    CHEVRON_3 = auto()
    CHEVRON_4 = auto()
    COMMUNICATIONS_ON = auto()
    COMMUNICATIONS_OFF = auto()
    OMEGA = auto()
    ORDER_1 = auto()
    ORDER_2 = auto()
    ORDER_3 = auto()
    ORDER_4 = auto()
    ORDER_5 = auto()

    @property
    def image_path(self):
        return os.path.join(TOKENS_ASSETS_FOLDER, self.name.lower() + ".png")


class CardTokenPosition(Enum):
    TOP = 0
    MIDDLE = 1
    BOTTOM = 2

    HIGHEST = 0
    SINGLE = 1
    LOWEST = 2

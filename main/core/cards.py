#!/usr/bin/python3
# -*- coding: utf-8 -*-

# BestCrewMate: a bot player for the board game The Crew 
# Copyright (C) 2021  Kasonnara <bcm@kasonnara.fr>
# GNU General Public License version 3 

"""
Representation of physical game cards
"""
import functools
import os
from dataclasses import dataclass
from enum import Enum, auto, unique

from random import Random
from typing import Union

from core.tokens import CardTokenPosition, CardToken


@unique
class CardColor(Enum):
    BLUE = 0
    GREEN = 1
    PINK = 2
    YELLOW = 3
    BLACK = 4


@unique
@functools.total_ordering
class Card(Enum):
    BLUE_1 = 0
    BLUE_2 = auto()
    BLUE_3 = auto()
    BLUE_4 = auto()
    BLUE_5 = auto()
    BLUE_6 = auto()
    BLUE_7 = auto()
    BLUE_8 = auto()
    BLUE_9 = auto()
    GREEN_1 = auto()
    GREEN_2 = auto()
    GREEN_3 = auto()
    GREEN_4 = auto()
    GREEN_5 = auto()
    GREEN_6 = auto()
    GREEN_7 = auto()
    GREEN_8 = auto()
    GREEN_9 = auto()
    PINK_1 = auto()
    PINK_2 = auto()
    PINK_3 = auto()
    PINK_4 = auto()
    PINK_5 = auto()
    PINK_6 = auto()
    PINK_7 = auto()
    PINK_8 = auto()
    PINK_9 = auto()
    YELLOW_1 = auto()
    YELLOW_2 = auto()
    YELLOW_3 = auto()
    YELLOW_4 = auto()
    YELLOW_5 = auto()
    YELLOW_6 = auto()
    YELLOW_7 = auto()
    YELLOW_8 = auto()
    YELLOW_9 = auto()
    BLACK_1 = auto()
    BLACK_2 = auto()
    BLACK_3 = auto()
    BLACK_4 = auto()

    def __init__(self, value):
        self.color: CardColor = CardColor(value // 9)
        self.number = (value % 9) + 1

        # Ensure color, number, value and name match
        assert 0 < self.number <= (9 if self.color != CardColor.BLACK else 4)
        assert self.name == "{}_{}".format(self.color.name, self.number)

    @property
    def image_path(self):
        return os.path.join('cards', self.name.lower() + ".png")

    @property
    def task_image_path(self):
        # FIXME make assets
        #return os.path.join('cards', self.name.lower() + "_task.png")
        return self.image_path

    def __lt__(self, other: 'CardLike'):
        """'Lower than' operator by color AND THEN by number. It's mainly used for sorting cards for display."""
        return self.value < other.value
        # Should be equivalent to:
        # return self.color.value < other.color.value or (self.color is other.color and self.number < other.number)

    def __eq__(self, other: 'CardLike'):
        """'Equals' implementation that should be compatible with any 'CardLike' object, a True means both object
        represent the same card, however this doesn't take into account extra metadata that may be added by other
        'CardLike' classes"""
        # Even so it's an enum, we don't use 'is' because we want to support any CardLike instances, like TokenizedCard.
        return self.color is other.color and self.number is other.number


@dataclass
@functools.total_ordering
class TokenizedCard:
    card: Card
    token: CardToken
    token_position: CardTokenPosition

    @property
    def color(self):
        return self.card.color

    @property
    def number(self):
        return self.card.number

    @property
    def value(self):
        return self.card.value

    @property
    def image_path(self):
        return self.card.image_path

    @property
    def task_image_path(self):
        return self.card.task_image_path

    def __lt__(self, other: 'CardLike'):
        return self.card < other

    def __eq__(self, other: 'CardLike'):
        if isinstance(other, Card):
            return self.card is other
            # Note: If in the future we add other types of CardLike objects, we may want to replace the 'is' operator
            #       by an '==' to use the Card.__eq__ with is compatible with any CardLike classes.
        if isinstance(other, TokenizedCard):
            return (self.card is other.card
                    and self.token is other.token
                    and self.token_position is other.token_position)
        assert False


def deal_cards(player_count: int):
    assert 0 < player_count <= 6
    deck = list(Card.__members__.values())
    Random().shuffle(deck)
    return [sorted([deck[i] for i in range(player, len(deck), player_count)]) for player in range(player_count)]


CardLike = Union[Card, TokenizedCard]


if __name__ == '__main__':
    # TODO move to test
    assert len(Card.__members__) == 40

    print("Colors:")
    for color in CardColor:
        print("- {} (id={})".format(color.name, color.value))
    print("Deck:")
    for card in Card:
        print("- {}=[{}:{}] (id={}) ".format(card.name, card.color, card.number, card.value))


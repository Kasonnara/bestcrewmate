#!/usr/bin/python3
# -*- coding: utf-8 -*-

# BestCrewMate: a bot player for the board game The Crew 
# Copyright (C) 2021  Kasonnara <bcm@kasonnara.fr>
# GNU General Public License version 3 

"""
Game modeling classes
"""
from dataclasses import dataclass
from typing import List

from core.cards import Card, deal_cards
from core.player import Player
from gui.common import MIN_PLAYER_COUNT, MAX_PLAYER_COUNT


class GameState:
    turn: int = 0
    current_turn_cards: List[Card] = []
    current_turn_first_player_index: int
    players: List[Player]

    def __init__(self, players: List[Player]):
        assert players is not None, "Player list can't be None"
        assert MIN_PLAYER_COUNT <= len(players) <= MAX_PLAYER_COUNT
        self.players = players

        # If players don't have hand, generate it
        if players[0].hand is None:
            assert all(player.hand is None for player in players)
            for player, random_hand in zip(players, deal_cards(len(players))):
                player.hand = random_hand

        # Game rule: the captain is the first player to play
        for i, player in enumerate(players):
            if player.isCaptain:
                self.current_turn_first_player_index = i
                break
        else:
            raise AssertionError("There is no player in the given player list: " + ", ".join([str(player) for player in players]))

    @property
    def current_player_index(self) -> int:
        return self.current_turn_first_player_index + len(self.current_turn_cards)

    @property
    def current_player(self) -> Player:
        return self.players[self.current_player_index]


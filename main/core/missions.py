#!/usr/bin/python3
# -*- coding: utf-8 -*-

# BestCrewMate: a bot player for the board game The Crew 
# Copyright (C) 2021  Kasonnara <bcm@kasonnara.fr>
# GNU General Public License version 3 

"""
Modeling of game missions levels
"""
import os.path
from abc import abstractmethod, ABC
from collections import defaultdict
from dataclasses import dataclass
from enum import Enum
from typing import Type, Iterable, Dict

from core.cards import Card
from core.tokens import CardToken

MISSION_ASSETS_FOLDER = 'missions'


class Task(ABC):
    @staticmethod
    @abstractmethod
    def icon_path(count: int):
        pass

    @staticmethod
    @abstractmethod
    def task_description(count: int):
        pass

    @staticmethod
    def extended_task_description(count: int):
        return None


class CardTask(Task):
    @staticmethod
    def task_description(count: int):
        assert 1 <= count <= 10
        return "The mission require to resolve {} tasks".format(count)

    @staticmethod
    def icon_path(count: int):
        assert 1 <= count <= 9
        # TODO use the proper icon
        return Card(count - 1).task_image_path


class PriorityTask(CardTask):
    @staticmethod
    def task_description(count: int):
        assert 1 <= count <= 5
        return "The task tagged with this token must be resolved " + ["first", "second", "third", "fourth", "fifth"][count-1]

    @staticmethod
    def icon_path(count: int):
        assert 1 <= count <= 5
        return CardToken(CardToken.ORDER_1.value + (count - 1)).image_path


class ChevronTask(CardTask):
    @staticmethod
    def task_description(count: int):
        assert 1 <= count <= 4
        return "The task tagged with this token must be resolved " + ("after the one tagged with " + '>' * (count-1) if count > 1 else "before any other task tagged with chevrons")

    @staticmethod
    def icon_path(count: int):
        assert 1 <= count <= 4
        return CardToken(CardToken.CHEVRON_1.value + (count - 1)).image_path


class OmegaTask(CardTask):
    @staticmethod
    def task_description(count: int):
        assert count == 1
        return "The task tagged with this token must be resolved last"

    @staticmethod
    def icon_path(count: int):
        assert count == 1
        return CardToken.OMEGA.image_path


class CommunicationPerturbation(Enum):
    # TODO better assets
    NONE = os.path.join('tokens', "communications_on.png"), "Communications are fine"
    SILENCE_ZONE = os.path.join('tokens', "communications_off.png"), "Zone de silence: vous pouvez communiquer, mais vous ne pouvez pas préciser le type de communication (plus haute carte, seule carte, plus basse carte)."
    SILENCED_PLAYER = os.path.join('tokens', "communications_off.png"), "Le membre d'équipage selectionné par le commandant, doit se concentrer. Il ne pourra pas communiquer du tout durant la mission."
    DELAYED_COMMUNICATION = os.path.join('tokens', "communications_off.png"), "Les communications sont retardées: vous ne pourrez communiquer qu'a partir du 2e pli (inclu)."
    DELAYED_COMMUNICATION_2 = os.path.join('tokens', "communications_off.png"), "Les communications sont retardées: vous ne pourrez communiquer qu'a partir du 3e pli (inclu)."

    def __init__(self, icon_path, description: str):
        self.icon_path = icon_path
        self.description = description


# ===== Special Tasks =====

class SickPlayerTask(Task):
    @staticmethod
    def task_description(count: int):
        return "A crew member feel sick, he must not win any folds."

    @staticmethod
    def extended_task_description(count: int):
        return "At the beginning of the mission, each crew member consult his hand. Then they indicate one by one if they feel sick, but they can only answer by \"good\" or \"bad\". The commander then decide who's sick. He can't select himself. The designed crew member must not win any folds in the mission."

    @staticmethod
    def icon_path(count: int):
        return os.path.join(MISSION_ASSETS_FOLDER, "biohazard-icon-fluo.svg")


class WinWithOnes(Task):
    @staticmethod
    def task_description(count: int):
        assert 1 <= count <= 4
        if count == 4:
            return "All color cards of value 1 must win the folds they are in"
        else:
            return f"At least {count} folds must be won by color cards of value 1."

    @staticmethod
    def icon_path(count: int):
        return os.path.join(MISSION_ASSETS_FOLDER, "AllOnesCards.png")


# ===== Mission =====

@dataclass
class Mission:
    tasks: Iterable[Type[Task]]
    communication_perturbation: CommunicationPerturbation
    isTaskDistributedByCommandant: bool

    """A short role play presentation of the mission in the game lore"""
    lore: str

    def get_task_levels(self):
        task_levels: Dict[Type[Task], int] = defaultdict(int)
        for task in self.tasks:
            # Count CardTask in a special way: all sub class of CardTask increase the CardTask counter.
            if issubclass(task, CardTask):
                task_levels[CardTask] += 1
            # CardTask subclasses and all other types also increment their own counter
            if task != CardTask:
                task_levels[task] += 1
        return task_levels


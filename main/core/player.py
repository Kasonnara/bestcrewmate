#!/usr/bin/python3
# -*- coding: utf-8 -*-

# BestCrewMate: a bot player for the board game The Crew 
# Copyright (C) 2021  Kasonnara <bcm@kasonnara.fr>
# GNU General Public License version 3 

"""
Base classes representing agents participating in the game (a human or an IA)
"""
from dataclasses import dataclass, field
from typing import List, Union, Optional

from core.cards import Card, TokenizedCard, CardColor, CardLike
from core.strategies import Strategy
from core.tokens import CardToken, CardTokenPosition


@dataclass
class Player:
    strategy: Strategy
    hand: List[CardLike]
    canCommunicate: bool = field()
    """True = Player can communicate, None = Player has communicated, False = Player can no longer communicate """
    isCaptain: bool = field(init=False)

    def __post_init__(self):
        self.isCaptain = Card.BLACK_4 in self.hand  # todo handle TokenizedCard properly

    def communicate(self, card: Union[int, Card], position: CardTokenPosition):
        assert self.can_communicate_card
        if type(card) is int:
            i, card = card, self.hand[card]
        else:
            assert type(card) is Card
            i, card = self.hand.index(card), card
        self.hand[i] = TokenizedCard(
            card,
            CardToken.COMMUNICATIONS_ON if position != None else CardToken.COMMUNICATIONS_OFF,
            position or position.MIDDLE)
        self.canCommunicate = None

    def can_communicate_card(self, card: Card) -> Optional[CardTokenPosition]:
        """
        Validate if the player can communicate the given card
        :param card: the car the player intend to communicate
        :return: None: if the player is not allowed to communicate this card (because it can't actually communicate
                    any card, or because the card is a black one, or because it doesn't fit any of the positions)
                 Else the player is allowed to communicate that card: return the position at which it can communicate.
        """
        if not self.canCommunicate:
            return None
        assert not isinstance(card, TokenizedCard)
        assert card in self.hand
        if card.color == CardColor.BLACK:
            return None

        same_color_cards = [hand_card.card
                            for hand_card in self.hand
                            if card.color == hand_card.color]
        if len(same_color_cards) == 1:
            return CardTokenPosition.SINGLE
        same_color_cards.sort()
        if same_color_cards[0] is card:
            return CardTokenPosition.LOWEST
        if same_color_cards[-1] is card:
            return CardTokenPosition.HIGHEST


@dataclass
class DisplayablePlayer(Player):
    pseudo: str
    color: str

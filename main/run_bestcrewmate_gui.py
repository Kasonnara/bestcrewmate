#!/usr/bin/python3
# -*- coding: utf-8 -*-

# BestCrewMate: a bot player for the board game The Crew 
# Copyright (C) 2021  Kasonnara <bcm@kasonnara.fr>
# GNU General Public License version 3 

"""
Main script of the dash application
"""

import dash
import dash_bootstrap_components as dbc
from dash import html

# Init the main application object (instantiated early in order to allow creating callbacks)
import gui.board
from application_singleton import app

app.layout = gui.board.GameBoardGui().root_widget


if __name__ == '__main__':
    app.run_server(debug=False)

#!/usr/bin/python3
# -*- coding: utf-8 -*-

# BestCrewMate: a bot player for the board game The Crew 
# Copyright (C) 2021  Kasonnara <bcm@kasonnara.fr>
# GNU General Public License version 3 

"""
Common constants and utility functions
"""
import math

from core.cards import Card

MIN_PLAYER_COUNT, MAX_PLAYER_COUNT = 3, 6

MAX_CARD_PER_PLAYER = int(math.ceil(len(Card) / MIN_PLAYER_COUNT))

playerColors = ["red", "green", "blue", "yellow", "purple", "orange"]
assert len(playerColors) >= MAX_PLAYER_COUNT

mock_player_names = ["Alice", "Bob", "Camille", "Daniel", "Edvige", "Françoise"]

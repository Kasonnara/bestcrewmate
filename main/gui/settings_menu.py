#!/usr/bin/python3
# -*- coding: utf-8 -*-

# BestCrewMate: a bot player for the board game The Crew 
# Copyright (C) 2021  Kasonnara <bcm@kasonnara.fr>
# GNU General Public License version 3 

"""
The gui for configuring game settings
"""
from enum import Enum
from typing import List

import dash_bootstrap_components as dbc
from dash import dcc
from dash import html
from dash import Dash
from dash.dependencies import Input, Output, State

from application_singleton import app
from core.cards import Card, deal_cards
from core.game import GameState
from core.player import Player, DisplayablePlayer
from gui.common import playerColors, MAX_PLAYER_COUNT, MIN_PLAYER_COUNT, mock_player_names
from gui.components.tokens import create_task_widgets, create_communication_widgets
from vanilla.missions import VANILLA_MISSIONS


class PlayerType(Enum):
    MYSELF = "Myself"
    #HUMAN = "Human"
    IA = "IA"

    @classmethod
    def get_dropdown_options(cls, is_myself: bool, is_myself_available: bool):
        # TODO: can be optimized by memoization or just hardcoding the 3 possible values
        return [
            {'label': option.value, 'value': option.name}
            for option in cls
            if is_myself_available or is_myself or option != cls.MYSELF
            ]


class PlayerSettingWidget(dbc.InputGroup):
    player_names = mock_player_names[:MAX_PLAYER_COUNT].copy()
    player_owners = [None] * MAX_PLAYER_COUNT

    def __init__(self, player_index: int):
        self.player_index = player_index

        super().__init__([
            dbc.InputGroupText(self.get_player_name(),
                               style={
                                   'color': self.get_color(),
                                   # Fix text area size for all players
                                   'width': "10rem",
                                   },
                               id=self.get_player_name_label_id(player_index),
                               ),
            dbc.Select(
                options=PlayerType.get_dropdown_options(False, True),
                value=PlayerType.IA.name,
                #clearable=False,
                id=self.get_player_type_dropdown_id(player_index),
                ),
            dbc.InputGroupText("Reserved by another player", id=self.get_reserved_label_id(player_index)),
            ],
            id=self.get_player_setting_widget_id(player_index),
            #style={'display': 'flex', 'flex-direction': 'row'},
            )

    def get_color(self):
        return playerColors[self.player_index]

    def get_player_name(self):
        return PlayerSettingWidget.player_names[self.player_index]

    @staticmethod
    def get_player_type_dropdown_id(player_index: int):
        return "player_type_dropdown_" + str(player_index)

    @staticmethod
    def get_player_setting_widget_id(player_index: int):
        return "player_setting_widget_" + str(player_index)

    @staticmethod
    def get_reserved_label_id(player_index: int):
        return "player_reserved_label_" + str(player_index)

    @staticmethod
    def get_player_name_label_id(player_index: int):
        return "player_name_label_" + str(player_index)

    def generate_player(self, starting_hand: List[Card]) -> Player:
        return DisplayablePlayer(None, starting_hand, True, self.get_player_name(), self.get_color())


MISSION_SELECT_ID = "mission_dropdown"
MISSION_LORE_WIDGET_ID = "mission-lore"
MISSION_ICONS_WIDGET_ID = "mission-icons"
USERNAME_ID = "username_input"
PLAYER_SYNC_INTERVAL_ID = 'player_sync_interval'

SYNC_INTERVAL = 3 * 1000  # in milliseconds


class SettingMenu:
    player_count = MIN_PLAYER_COUNT
    is_game_started = False

    def __init__(self, game_board: 'GameBoardGui'):
        self.player_setting_widgets = [PlayerSettingWidget(i) for i in range(MAX_PLAYER_COUNT)]

        self.root_widget = dbc.Modal([
                dbc.ModalHeader(html.H1("Game settings"), close_button=False),
                dbc.ModalBody([
                    dbc.Input(id=USERNAME_ID, name="username", placeholder="pseudo", persistence=True),
                    html.H2([
                        html.Span("Players ", style={'width': '70%'}),
                        dcc.Dropdown(
                            options=[{'label': str(n), 'value': str(n)}
                                     for n in range(MIN_PLAYER_COUNT, MAX_PLAYER_COUNT + 1)],
                            value=str(SettingMenu.player_count),
                            id="player_count_dropdown",
                            #style={'width': '20%'},
                            clearable=False,
                            ),
                        ],
                        style={'display': 'flex', 'flex-direction': 'row'}),
                    *self.player_setting_widgets,

                    html.H2([
                        html.Span("Mission ", style={'width': '70%'}),
                        dcc.Dropdown(
                            options=[{'label': str(n + 1), 'value': str(n)}
                                     for n in range(len(VANILLA_MISSIONS))],
                            value="0",
                            id=MISSION_SELECT_ID,
                            # style={'width': '20%'},
                            clearable=False,
                            ),
                        ],
                        style={'display': 'flex', 'flex-direction': 'row'}),

                        dcc.Markdown("", id=MISSION_LORE_WIDGET_ID),
                        html.Div([],
                                id=MISSION_ICONS_WIDGET_ID,
                                style={"height": "5em", 'width': "100%", 'flex-direction': 'row',
                                       # Center content
                                       'display': 'flex', 'justify-content': 'center'},
                                ),
                    ]),

                dbc.ModalFooter([
                    dbc.Button("Start game", color='primary', id='start_game_button'),
                    ]),

                # --- Utility invisible element ---
                dcc.Interval(
                    id=PLAYER_SYNC_INTERVAL_ID,
                    interval=SYNC_INTERVAL,  # in milliseconds
                    ),
                # These hidden div are only here to serve as a mock Output for callback that don't need outputs
                html.Div(id="no-output-hack", style={'display': 'none'}),
                html.Div(id="no-output-hack-2", style={'display': 'none'}),
            ],
            is_open=True,  # Open by default when the user connects
            centered=True,
            backdrop='static',  # Cannot be skipped
            keyboard=False,  # Cannot be skipped
            #fullscreen=True,
            id="settings_menu",
            )

        @app.callback(
            Output("no-output-hack", 'hidden'),
            Input("player_count_dropdown", component_property='value'),
            )
        def on_player_count_edited(new_player_count):
            if new_player_count is not None:
                SettingMenu.player_count = int(new_player_count)
            return True

        @app.callback(
            Output("player_count_dropdown", component_property='value'),
            *[Output(player_setting_widget.id, component_property="hidden") for player_setting_widget in
              self.player_setting_widgets],
            *[Output(component_id, 'hidden')
              for player_index in range(MAX_PLAYER_COUNT)
              for component_id in (PlayerSettingWidget.get_player_type_dropdown_id(player_index),
                                     PlayerSettingWidget.get_reserved_label_id(player_index))
              ],
            Input(PLAYER_SYNC_INTERVAL_ID, 'n_intervals'),
            State(USERNAME_ID, 'value'),
            )
        def on_player_count_change(n_intervals, username):
            #print("Retrieveing SettingMenu.player_count", SettingMenu.player_count)
            reserved_players = [owner is None or owner == username
                                for owner in PlayerSettingWidget.player_owners]

            return [str(SettingMenu.player_count)] \
                + [(False if i < SettingMenu.player_count else True)
                    for i in range(MAX_PLAYER_COUNT)] \
                + [x for is_reserved in reserved_players for x in (not is_reserved, is_reserved)]

        @app.callback(
            *[Output(PlayerSettingWidget.get_player_name_label_id(i), 'value') for i in range(MAX_PLAYER_COUNT)],
            Input(PLAYER_SYNC_INTERVAL_ID, 'n_intervals'),
            )
        def refresh_player_names(n_intervals):
            return PlayerSettingWidget.player_names

        @app.callback(
            Output("no-output-hack-2", 'hidden'),
            *[Input(PlayerSettingWidget.get_player_type_dropdown_id(player_index), 'value') for player_index in range(MAX_PLAYER_COUNT)],
            State(USERNAME_ID, 'value'),
            )
        def on_player_type_edited(*args):
            dropdown_values, username = args[:-1], args[-1]
            for i, owner, new_dropdown_value in zip(range(MAX_PLAYER_COUNT), PlayerSettingWidget.player_owners, dropdown_values):
                if owner == username:
                    # Owned by this user, so it has control
                    if new_dropdown_value != PlayerType.MYSELF.name:
                        PlayerSettingWidget.player_owners[i] = None
                        PlayerSettingWidget.player_names[i] = mock_player_names[i]
                if owner is None:
                    # Owned by nobody, so every body has control
                    if new_dropdown_value == PlayerType.MYSELF.name:
                        PlayerSettingWidget.player_owners[i] = username
                        PlayerSettingWidget.player_names[i] = username
            return True

        @app.callback(
            *[Output(PlayerSettingWidget.get_player_type_dropdown_id(i), 'options') for i in range(MAX_PLAYER_COUNT)],
            Input(PLAYER_SYNC_INTERVAL_ID, 'n_intervals'),
            State(USERNAME_ID, 'value'),
            )
        def on_player_type_change(n_intervals, username):
            is_myself_available = username is not None and len(username) > 0 and username not in PlayerSettingWidget.player_owners
            #print("sync: is_myself_available", is_myself_available, "username =", username)
            return [
                PlayerType.get_dropdown_options(PlayerSettingWidget.player_owners[i] == username, is_myself_available)
                for i in range(MAX_PLAYER_COUNT)
                ]

        @app.callback(
            Output(MISSION_LORE_WIDGET_ID, 'children'),
            Output(MISSION_ICONS_WIDGET_ID, 'children'),
            Input(MISSION_SELECT_ID, 'value')
            )
        def on_mission_change(selected_mission: str):
            selected_mission = int(selected_mission)
            mission = VANILLA_MISSIONS[selected_mission]
            return mission.lore, [
                       widget
                        for taskType, task_level in mission.get_task_levels().items()
                        for widget in create_task_widgets(taskType, task_level)
                   ] + create_communication_widgets(mission.communication_perturbation)

        @app.callback(
            # Setting menu to disable
            Output(PLAYER_SYNC_INTERVAL_ID, 'disabled'),
            Output(USERNAME_ID, 'disabled'),
            Output("player_count_dropdown", 'disabled'),
            Output(MISSION_SELECT_ID, 'disabled'),
            *[Output(player_setting_widget.id, 'disabled') for player_setting_widget in self.player_setting_widgets],
            # Setting modal
            Output("settings_menu", 'is_open'),
            # Game board to enable
            Output("board_sync_interval", 'disabled'),
            Output("board_sync_interval", 'max_intervals'),

            # Start button
            Input('start_game_button', 'n_clicks'),
            )
        def on_game_start(start_button_n_clicks):
            if start_button_n_clicks is None:
                # It's start up unwanted trigger, ignore it
                return (
                    [False] * (4 + len(self.player_setting_widgets))
                    + [True]
                    + [True, 0]
                    )

            print("Game start")
            # Generate a new game state
            game_board.game_state = self.generate_game_state()

            # Update dash components
            return (
                # Setting menu to disable
                [True] * (4 + len(self.player_setting_widgets))
                # Setting modal
                + [False]
                # Game board to enable
                + [False, -1]
                )

    def generate_game_state(self) -> GameState:
        print("Generating a new game")
        return GameState(
                [player_widget.generate_player(hand)
                 for player_widget, hand in zip(self.player_setting_widgets[:SettingMenu.player_count], deal_cards(SettingMenu.player_count))]
            )

#!/usr/bin/python3
# -*- coding: utf-8 -*-

# BestCrewMate: a bot player for the board game The Crew
# Copyright (C) 2021  Kasonnara <bcm@kasonnara.fr>
# GNU General Public License version 3

"""
Gui of the game board
"""
import math
import os
import random
from typing import Optional

import dash
import dash_bootstrap_components as dbc
from dash import dcc, html, Dash
from dash.dependencies import Input, Output, State

from application_singleton import app
from core.cards import deal_cards, Card
from core.game import GameState
from core.player import Player, DisplayablePlayer, TokenizedCard, CardTokenPosition
from gui.common import playerColors, mock_player_names
from gui.components.card import CardSetWidget
from gui.components.player import PlayerWidget
from gui.settings_menu import SettingMenu, USERNAME_ID, SYNC_INTERVAL

class GameBoardGui:
    BOARD_MAIN_CONTAINER_ID = "board_main_container"

    def __init__(self):
        self.game_state: Optional[GameState] = None

        self.setting_menu = SettingMenu(self)
        self.board_main_container = dbc.Col(
            [],
            className='d-flex flex-column',
            id=self.BOARD_MAIN_CONTAINER_ID,
            style={
                'height': '100%',
                'padding': '8px',
                },
            )
        board_sync_interval = dcc.Interval(
            id="board_sync_interval",
            interval=SYNC_INTERVAL,  # in milliseconds
            disabled=True,  # disabled at the beginning, it will be enabled when the game really starts
            max_intervals=0,  # disabled at the beginning, it will be enabled when the game really starts
            )
        # Put a background image filling the whole screen and not scrolling with the rest of the content
        board_background = dbc.Container(
            [self.board_main_container,
             self.setting_menu.root_widget,
             board_sync_interval,
             ],
            id='board_background',
            style={
                "background-image": "url(\"" + app.get_asset_url(os.path.join('backgrounds', 'default.jpg')) + "\")",
                "background-size": 'cover',
                'background-position': 'center',
                "background-repeat": 'no-repeat',
                'height': '100vh',
                # 'max-height': '100vh',
                # 'overflow': 'auto',
                'background-attachment': 'fixed',
                },
            fluid=True,
            )
        self.root_widget = board_background
        """The top level dash widget holding all the board gui"""

        @app.callback(
            Output(GameBoardGui.BOARD_MAIN_CONTAINER_ID, 'children'),
            Input("board_sync_interval", 'n_intervals'),
            State(USERNAME_ID, 'value'),
            )
        def update_board_callback(n_intervals, username):
            return self.update_board(username)

    def update_board(self, username: Optional[str]):
        if self.game_state is None:
            print("skip refresh board for user: " + username)
            return []
        print("refresh board for user: " + username)
        # Check if user is in player list
        player_names = [player.pseudo for player in self.game_state.players]
        is_observer_mode = username not in player_names

        # Organize players
        if not is_observer_mode:
            # current username is a player
            # so put it on the bottom and all others on top
            user_index = player_names.index(username)
            top_players = self.game_state.players[user_index + 1:] + self.game_state.players[:user_index]
            bottom_players = (self.game_state.players[user_index],)
        else:
            # current username is an observer
            # distribute players equally between top and bottom
            median_index = math.ceil(len(self.game_state.players) / 2)
            top_players = self.game_state.players[:median_index]
            bottom_players = list(reversed(self.game_state.players[median_index:]))

        # Generate player widgets
        top_players_areas = [
                html.Div(PlayerWidget(player, not is_observer_mode),
                        id="player-" + player.pseudo + "-area",
                        className='col p-0 ' + ('mr-1' if i == 0 else 'ml-1' if i == len(top_players) - 1 else 'mx-1'),
                        style={'height': '100%', 'width': str(100//(len(top_players)))+'%'})
                for i, player in enumerate(top_players)
            ]
        bottom_players_areas = [
            html.Div(PlayerWidget(player, False),
                     id="player-" + player.pseudo + "-area",
                     className='col p-0 ' + ('mr-1' if i == 0 else 'ml-1' if i == len(bottom_players) - 1 else 'mx-1'),
                     style={'height': '100%', 'width': str(100 // (len(bottom_players))) + '%'})
            for i, player in enumerate(bottom_players)
            ]

        current_turn_area = html.Div(
            [
                html.H1("turn"),
                CardSetWidget(self.game_state.current_turn_cards),
                ],
            className='board-area-boundaries m-0', #flex-grow-1
            )

        return [
            dbc.Row(top_players_areas, className="my-1", style={'height': '32%'}),
            dbc.Row([current_turn_area], className="my-1", style={'height': '32%'}),
            dbc.Row(bottom_players_areas, className="my-1", style={'height': '32%'}),
            ]

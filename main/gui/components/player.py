#!/usr/bin/python3
# -*- coding: utf-8 -*-

# BestCrewMate: a bot player for the board game The Crew 
# Copyright (C) 2021  Kasonnara <bcm@kasonnara.fr>
# GNU General Public License version 3 


"""
Dash component(s) to display a player
"""
import os

from dash import Dash
from dash import html, dcc
import dash_bootstrap_components as dbc

from application_singleton import app
from core.player import DisplayablePlayer, TokenizedCard
from gui.components.card import CardSetWidget
from gui.components.tokens import create_player_communication_widgets, create_captain_widget


class PlayerWidget(html.Div):
    def __init__(self, player: DisplayablePlayer, is_hand_hidden: bool):

        player_communication_icon = create_player_communication_widgets(
            player.canCommunicate,
            "player_coms_"+player.color,
            style={'height': "1.2em"}
            )
        captain_icon = create_captain_widget(
            "player_captain_"+player.color,
            is_captain=player.isCaptain,
            style={'height': "1.2em"}
            )

        displayed_hand = [card for card in player.hand if not is_hand_hidden or type(card) is TokenizedCard]
        displayed_hand.sort()
        displayed_hand = ([None] * (len(player.hand) - len(displayed_hand))) + displayed_hand

        super().__init__([
        dbc.Col([
            html.H6([player.pseudo + " "] + player_communication_icon + captain_icon,
                    className='card-subtitle',
                    style={
                        'color': player.color,
                    }),
            CardSetWidget(displayed_hand)
            ],
            style={
                'height': '100%',
                }
            ),
        ],
        className="board-area-boundaries m-0 p-3",
        style={
            'height': '100%',
            'border-color': player.color,
            })

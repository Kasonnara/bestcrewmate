#!/usr/bin/python3
# -*- coding: utf-8 -*-

# BestCrewMate: a bot player for the board game The Crew 
# Copyright (C) 2021  Kasonnara <bcm@kasonnara.fr>
# GNU General Public License version 3 

"""
Token widgets creations
"""
import os

import dash_bootstrap_components as dbc
from dash import html
from dash import Dash
from dash.development.base_component import Component

from typing import Type, Iterable, List, Optional

from application_singleton import app
from core.missions import CardTask, Task, CommunicationPerturbation
from core.tokens import CardToken, TOKENS_ASSETS_FOLDER
from gui.components.card import CARD_ASPECT_RATIO


def create_task_widgets(task_type: Type[Task], task_level: int) -> Iterable[Component]:
    widgets = []
    for cur_task_level in (range(1, task_level + 1) if task_type != CardTask else [task_level]):
        widgets += create_token_widgets(
            task_type.icon_path(cur_task_level),
            task_type.task_description(cur_task_level),
            f"{task_type.__name__}_{cur_task_level}_icon",
            style={'max-height': "100%"},
            )
    return widgets


def create_communication_widgets(communication_perturbation: CommunicationPerturbation) -> List[Component]:
    if communication_perturbation != CommunicationPerturbation.NONE:
        return create_token_widgets(
            communication_perturbation.icon_path,
            communication_perturbation.description,
            f"{communication_perturbation.name}_icon",
            style={'max-height': "100%"},
            )
    else:
        return []


def create_player_communication_widgets(player_can_communicate: Optional[bool], widget_id, style={}) -> List[Component]:
    if player_can_communicate is None:
        # If player communication is None this means that player is currently
        # communicating, so the token is already placed on one of it's card and
        # shouldn't be displayed on the player itself.
        return []
    else:
        return create_token_widgets(
            (CardToken.COMMUNICATIONS_ON if player_can_communicate else CardToken.COMMUNICATIONS_OFF).image_path,
            ("Player can communicate." if player_can_communicate else "Player can't communicate."),
            widget_id,
            style=style,
            )


def create_captain_widget(widget_id, is_captain=True, style={}) -> List[Component]:
    if is_captain:
        return create_token_widgets(
            os.path.join(TOKENS_ASSETS_FOLDER, 'captain.png'),
            "This player is the captain during this mission (e.g. he owns the 4th rocket(black) card).",
            widget_id,
            style=style,
            )
    else:
        return []


def create_token_widgets(token_icon_path: str, token_tooltip: str, widget_id, style={}) -> List[Component]:
    aspect_ratio = (CARD_ASPECT_RATIO if 'cards' in token_icon_path else "1 / 1")
    return [
        # Token image
        # Div wrapper is mandatory because ??? (margins or aspect ratio doesn't work with Img only?)
        html.Div(
            html.Img(src=app.get_asset_url(token_icon_path),
                     id=widget_id,
                     style={'height': "100%",
                            'aspect-ratio': aspect_ratio,
                            },
                     ),
            style={
                **style,
                # Erase some attribute mandatory for this widget to work properly
                'aspect-ratio': aspect_ratio,
                },
            className='m-1',
            ),

        # Token description tooltip
        dbc.Tooltip(
            token_tooltip,
            target=widget_id,
            placement='top',
            ),
        ]

#!/usr/bin/python3
# -*- coding: utf-8 -*-

# BestCrewMate: a bot player for the board game The Crew 
# Copyright (C) 2021  Kasonnara <bcm@kasonnara.fr>
# GNU General Public License version 3 

"""
Dash component(s) to display game cards
"""
import os.path
from typing import List, Union

from dash import Dash
from dash import html, dcc
import dash_bootstrap_components as dbc

from application_singleton import app
from core.cards import Card
from core.player import CardTokenPosition, TokenizedCard
from core.tokens import CardToken

card_back_path = os.path.join('cards', 'back.png')

CARD_ASPECT_RATIO = "657 / 1021"

class CardWidget(html.Img):
    def __init__(self, card=None, **kwargs):
        super().__init__(
            #src=app.get_asset_url(card_back_path),
            **kwargs)
        self.update_state(card)

    def update_state(self, card: Card):
        self.src = app.get_asset_url(card.image_path if card is not None else card_back_path)


class TokenizedCardWidget(html.Div):
    def __init__(self, card: TokenizedCard, style=None, **kwargs):
        full_style = {
            'aspect-ratio': CARD_ASPECT_RATIO,
            }
        if style is not None:
            full_style.update(style)

        super().__init__(
            children=[
                CardWidget(card.card,
                           style={
                               'height': "100%",
                               'aspect-ratio': CARD_ASPECT_RATIO,
                               'position': 'relative',
                               },
                           ),
                CardTokenWidget(card.token, card.token_position)
                ],
            style=full_style,
            **kwargs
            )


class CardTokenWidget(html.Img):
    def __init__(self, token: CardToken, token_position: CardTokenPosition):
        super().__init__(
            src=app.get_asset_url(token.image_path),
            style={
                'position': 'absolute',
                'width': '50%',
                'top': "{}%".format(int(50 - (25 / 2) + (token_position.value - 1) * 33)),
                'left': "50%",
                # Move images back a little to compensate for its own width
                'transform': "translate(-50%,0%)"
                },
            )


class CardSetWidget(html.Div):
    def __init__(self, cards: List[Union[Card, TokenizedCard]]):
        card_horizontal_offset_percent = 100 // (len(cards) + 1)
        super().__init__(
            children=[
                (CardWidget if type(card) is not TokenizedCard else TokenizedCardWidget)(
                    card,
                    style={
                        'top': "10%",
                        'max-height': "75%",
                        # Offset each card relatively to the deck not relatively to other card
                        'position': 'absolute',
                        'left': str((i + 1) * card_horizontal_offset_percent) + '%',
                        # Move images back a little to compensate for its own width
                        'transform': 'translate({}%,0%)'.format(-(i + 1) * card_horizontal_offset_percent),
                        })
                for i, card in enumerate(cards)
                ],
            className="m-0",
            style={
                'height': '100%',
                'position': 'relative',  # makes children (e.g. the cards) position relative to this
                })

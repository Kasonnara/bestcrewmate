#!/usr/bin/python3
# -*- coding: utf-8 -*-

# BestCrewMate: a bot player for the board game The Crew 
# Copyright (C) 2021  Kasonnara <bcm@kasonnara.fr>
# GNU General Public License version 3 

"""
A module containing the just Dash application singleton such that it can be imported and referenced anywhere
instead of passing it everywhere as parameter
"""
import dash
import dash_bootstrap_components as dbc

app = dash.Dash(__name__, external_stylesheets=[dbc.themes.BOOTSTRAP])
"""The main dash application object"""

app.title = "Best Crew Mate"
